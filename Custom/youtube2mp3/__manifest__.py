{
    'name': 'Youtube mp3 Downloader',
    'version': '1.0',
    'summary': 'Faça downloads do youtube em mp3 sem limites',
    'sequence': -20,
    'description': """módulo para baixar músicas""",
    'category': 'Productivity',
    'website': 'https://www.google.com',
    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,
    'depends': [
        'sale',
        'mail'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/yt2mp3_sequence.xml',
        'views/youtube2mp3.xml'
    ]
}
