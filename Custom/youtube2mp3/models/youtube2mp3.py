from odoo import fields, models, api, _


class Youtube2Mp3(models.Model):
    _name = "youtube2mp3"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _description = "Youtube para mp3"

    sigle_link = fields.Char(string="Link Único", tracking=True)
    playlist_link = fields.Char(string="Link da Playlist", tracking=True)
    howmany = fields.Integer(string="Quantidade de músicas", tracking=True)
    status = fields.Selection([("waiting", "Em espera"), ("loading", "Baixando"), ("done", "Pronto")],
                              string="Status", default="waiting", tracking=True)
    song_name = fields.Char(string="Nome da música", tracking=True, readonly=True)

    ref_sequence_songs = fields.Char(string='Referência', required=True, tracking=True,
                                     copy=False, readonly=True, default=lambda self: _('Novo'))

    def action_download(self):
        self.status = "loading"

    def action_done(self):
        self.status = "done"

    def action_check(self):
        self.song_name = "teste"

    @api.model
    def create(self, vals):
        if vals.get('ref_sequence_songs', _('Novo')) == _('Novo'):
            vals['ref_sequence_songs'] = self.env['ir.sequence'].next_by_code('yt2mp3.sequence') or _('Novo')
        res = super(Youtube2Mp3, self).create(vals)
        return res
