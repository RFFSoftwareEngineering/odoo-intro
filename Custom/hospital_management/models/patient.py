from odoo import fields, models, api, _


class HospitalPatient(models.Model):
    _name = "hospital.patient"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _description = "Hospital Patient"

    name = fields.Char(string='Nome', required=True, tracking=True)
    age = fields.Integer(string='Idade', tracking=True)
    gender = fields.Selection([
        ('male', 'Homem'),
        ('female', 'Mulher'),
        ('other', 'outro')
    ], required=True, default='male', string='Sexo', tracking=True)
    note = fields.Text(string='Descrição', tracking=True)
    state = fields.Selection([('pendent', 'Pendente'), ('confirm', 'Confirmado'), ('sent', 'Em Tratamento'),
                              ('done', 'Obteve Alta'), ('cancel', 'Cancelado')], string="Estado", default="pendent",
                             tracking=True)
    responsible_id = fields.Many2one('res.partner', string='Parente', tracking=True)
    # novo field para a sequência:
    ref_sequence_patient = fields.Char(string='Referência', required=True, tracking=True,
                                       copy=False, readonly=True, default=lambda self: _('Novo'))

    def action_confirm(self):
        self.state = "confirm"
        # para pega o valor do campo faça como abaixo:
        # x = self.note
        # print(x)

    def action_sent(self):
        self.state = "sent"

    def action_done(self):
        self.state = "done"

    def action_cancel(self):
        self.state = "cancel"

    def action_pendent(self):
        self.state = "pendent"

    @api.model
    def create(self, vals):
        if not vals.get('note'):
            vals['note'] = 'Novo Paciente (descrever o problema)'
        if vals.get('ref_sequence_patient', _('Novo')) == _('Novo'):
            vals['ref_sequence_patient'] = self.env['ir.sequence'].next_by_code('patient.sequence') or _('Novo')
        res = super(HospitalPatient, self).create(vals)
        return res
