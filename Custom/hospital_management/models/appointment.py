from odoo import fields, models, api, _


class HospitalAppointment(models.Model):
    _name = "hospital.appointment"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _description = "Hospital Patient Appointment"

    note = fields.Text(string='Descrição', tracking=True)

    patient_id = fields.Many2one('hospital.patient', string="Paciente", required=True)

    age = fields.Integer(string='Idade', related='patient_id.age')

    gender = fields.Selection([
        ('male', 'Homem'),
        ('female', 'Mulher'),
        ('other', 'outro')
    ], required=True, default='male', string='Sexo', tracking=True, related='patient_id.gender')

    state = fields.Selection([('pendent', 'Pendente'), ('confirm', 'Confirmado'), ('sent', 'Em Tratamento'),
                              ('done', 'Obteve Alta'), ('cancel', 'Cancelado')], string="Estado", default="pendent",
                             tracking=True)

    date_appointment = fields.Date(string="Data")
    datetime_appointment = fields.Datetime(string="Hora")

    # novo field para a sequência:
    ref_sequence_appointment = fields.Char(string='Referência', required=True, tracking=True,
                                           copy=False, readonly=True, default=lambda self: _('Novo'))

    def action_confirm(self):
        self.state = "confirm"
        # para pega o valor do campo faça como abaixo:
        # x = self.note
        # print(x)

    def action_sent(self):
        self.state = "sent"

    def action_done(self):
        self.state = "done"

    def action_cancel(self):
        self.state = "cancel"

    def action_pendent(self):
        self.state = "pendent"

    @api.model
    def create(self, vals):
        if not vals.get('note'):
            vals['note'] = 'Novo Paciente (descrever o problema)'
        if vals.get('ref_sequence_appointment', _('Novo')) == _('Novo'):
            vals['ref_sequence_appointment'] = self.env['ir.sequence'].next_by_code('appointment.sequence') or _('Novo')
        res = super(HospitalAppointment, self).create(vals)
        return res
