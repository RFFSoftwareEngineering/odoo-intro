# -*- coding: utf-8 -*-
{
    'name': 'Gerenciamento Hospitalar',
    'version': '1.0',
    'summary': 'Exemplo de Gerenciamento hospitalar',
    'sequence': 10,
    'description': """módulo de treino""",
    'category': 'Productivity',
    'website': 'https://www.google.com/search?q=gerenciamento+hospitalar',
    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,
    'depends': [
        'sale',
        'mail'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/patient_sequence.xml',
        'views/patient_view.xml',
        'views/kids_view.xml',
        'views/patient_gender_view.xml',
        'views/appointment_view.xml',
        'views/sale.xml'
    ]
}
